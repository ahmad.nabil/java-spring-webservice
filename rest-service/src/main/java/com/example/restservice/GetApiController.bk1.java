package com.example.restservice;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import firebasesvc.FirebaseService;

import org.springframework.beans.factory.annotation.Autowired;


@RestController
public class GetApiController {

	@Autowired
	FirebaseService fbs;
	
	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@GetMapping("/GetApi")
	public GetApi getapi(@RequestParam(value = "passkey", defaultValue = "NoPasskey") String passkeyName) {
		
		try {
			fbs.saveAllDetails();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new GetApi(counter.incrementAndGet(), String.format(template, passkeyName));
	}
}