package com.example.restservice;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;


/*import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import java.io.FileInputStream;
import org.springframework.web.client.RestTemplate;
import firebasesvc.FirebaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;*/


@RestController
public class GetApiController {	
	
	
		
	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@GetMapping("/GetApi")
	public GetApi getapi(@RequestParam(value = "passkey", defaultValue = "NoPasskey") String passkeyName) throws IOException {
						
			try {
				FileInputStream serviceAccount = new FileInputStream("C:\\code\\rest-service\\serviceAccount.json");
		

					FirebaseOptions options = new FirebaseOptions.Builder()
					  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
					  .setDatabaseUrl("https://paynet-test-103d3.firebaseio.com")
					  .build();

					FirebaseApp.initializeApp(options);
		
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		RestTemplate restTemplate = new RestTemplate();
		
		ApiFuture<DocumentReference> addedDocRef;
		
		Object[] forNow = restTemplate.getForObject("https://restcountries.eu/rest/v2/regionalbloc/asean", Object[].class);
	    var searchList= Arrays.asList(forNow);
	    	    
	    Firestore dbFirestore = FirestoreClient.getFirestore();
	    
	    Map<String, Object> data = new HashMap<>();
	    for (int i=0; i<searchList.size(); i++) 
	    {	
	    	data.put("name", searchList.toString());		    	
	    	addedDocRef = dbFirestore.collection("json_object").add(data);
	    }
		
		return new GetApi(counter.incrementAndGet(), String.format(template, "Data Inserted into Firebase"));
	}
}